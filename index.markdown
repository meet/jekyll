---
layout: start
title: Open source cooperative video conferencing platform, meet.coop
---

**[Join Meet.coop as a User Member](https://opencollective.com/meet-coop) to:**

- Enjoy GDPR compliant video conferencing, free from surveillance and monetisation of users’ data
- Accelerate the development of cooperatively owned and cooperatively run digital infrastructure
- Contribute to the developing commons-cooperative economy, facilitated by open source tools

Users can join at four levels:

| Level 1 | Level 2 | Level 3 | Multi-user Member |
| ------ | ------ | ------ | ------ |
| Maximum of 10 meeting participants | Maximum of 20 meeting participants | Maximum of 50 meeting participants | Unlimited user accounts, Max 100 meeting participants, Minimum of 3 months subscription |
| £9 / month | £18 / month | £40 / month | £90 / month |
| **[Join now](https://opencollective.com/meet-coop/contribute/member-level-1-19216/checkout)** | **[Join now](https://opencollective.com/meet-coop/contribute/member-level-2-19217/checkout)** | **[Join now](https://opencollective.com/meet-coop/contribute/member-level-3-19218/checkout)** | **[Join now](https://opencollective.com/meet-coop/contribute/mutli-user-member-19219/checkout)** |

<small>Prices are quoted in GBP, but will be charged in your local currency.</small>
<small>Please refer to our [terms](terms.html) for information about billing and fair use.</small>

### Dedicated Event Server

Our dedicated event servers are designed to support events with many online meetings that require recorded sessions. We have significant experience managing and hosting large events up to 200 people.

The price to hire our event server is £100/week plus a £100 setup fee, which we will waive if you hire the server for 4 weeks or more. The price includes 1 hour of support (extra hours billed at £60 per hour) and we require 1 week lead time to set up the server for you. Please [contact us](mailto:contact@meet.coop) if you are interested in hiring the event server, letting us know the size and dates and time of your event, and any other specific requirements, so that we can check availability and get you booked in.

### Dedicated Custom Server

For organisations with many online meetings, we provide dedicated custom servers that start at £250/month plus a £250 setup fee, which we will waive if you hire a custom server for 3 months or more. Please [contact us](mailto:contact@meet.coop) if you are interested in a setting up a custom server. We require 2 weeks lead time to set up the server at one of our datacentres in Canada, Germany, France, UK, or Poland.

---

<small>Find out more [about Meet.coop](https://www.org.meet.coop/about.html) and how you can [get involved](https://www.org.meet.coop/getinvolved.html) as an Operational Member, or chat with us in the [Meet.coop forum](https://forum.meet.coop/).</small>

<small>We are actively looking for investment and funding. If you would like to support Meet.coop you can [donate via OpenCollective](https://opencollective.com/meet-coop/donate), or contact us via [contact@meet.coop](mailto:contact@meet.coop) about other funding and investment ideas.</small>