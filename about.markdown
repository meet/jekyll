---
title: About Meet.coop
---

The Online Meeting Co-operative is a meeting and conferencing platform powered by [BigBlueButton](https://bigbluebutton.org), stewarded by our Operational Members from across the world.

Our organisation is a multi-stakeholder co-operative with [three sociocratic circles](https://hackmd.io/dvwznF1HTc6XuiLXZ5Sgaw?view), with operational members taking on specific roles to deliver reliable services to our [user membership](https://opencollective.com/meet-coop#section-contributors).

## Operational Members

<table>
	<tr>
		<td width="240"><a href="https://autonomic.zone" target="_blank"><img src="assets/logos/member-autonomic.png" /></a></td>
		<td>Autonomic is a worker-owned cooperative, dedicated to using technology to empower people making a positive difference in the world. Since 2017, we've been making websites, hosting tools and developing bespoke apps for organisations in a range of sectors including renewable energy, charities and labour unions.</td>
	</tr>
	<tr>
		<td width="240"><a href="https://femprocomuns.coop" target="_blank"><img src="assets/logos/member-femprocomuns.png" /></a></td>
		<td>femProcomuns is a non-profit and social initiative, worker and consumer multi-stakeholder cooperative, created in Catalonia in 2017, with the aim of consolidating a commons ecosystem, based on the principles of open cooperativism, community self-management, human, ecological, economic sustainability, shared knowledge and replicability.</td>
	</tr>
	<tr>
		<td width="240"><a href="http://freeknowledge.eu" target="_blank"><img src="assets/logos/member-fki.png" /></a></td>
		<td>The Free Knowledge Institute (FKI) is a non-profit organisation that fosters equal access to tools for production and exchange of knowledge in all areas of society. Inspired by the Free Software movement, the FKI promotes freedom of use, modification, copying and distribution of knowledge in several different but closely related fields. Accordingly, it promotes the commons economy. The FKI was established as a foundation in 2007 in Amsterdam and works as a distributed team between Rome, Barcelona and Amsterdam.</td>
	</tr>
	<tr>
		<td width="240"><a href="https://hypha.coop" target="_blank"><img src="assets/logos/member-hypha.png" /></a></td>
		<td>Hypha is a worker co-operative based in Toronto who helps organizations and communities redesign their relationships with digital technology. Their team of technologists, designers, and community organizers provide digital strategy and coaching, research, web and mobile design and development, and infrastructure deployment services.</td>
	</tr>
	<tr>
		<td width="240"><img src="assets/logos/member-melissa.jpg" /></td>
		<td>Melissa McNab is a Freelance Illustrator and Designer, Co-owner of Code-Operative Ltd; a non-profit service co-operative founded in 2018 to provide freelancers a better solution. She worked on projects such as designing the UX/UI for Acorn the Union Emergency Response App, Twine Volunteering Management App and a 3D world for the Greenham Common Womens Peace Camp 40th Anniversary.</td>
	</tr>
	<tr>
		<td width="240"><a href="https://open.coop" target="_blank"><img src="assets/logos/member-open.gif" /></a></td>
		<td>The Open Co-op was founded in 2004 with the purpose of "building a world-wide community of individuals and organisations committed to the creation of a collaborative, sustainable economy". We organise conferences and run projects to help create decentralised collaboration at scale.</td>
	</tr>
</table>

## Shared Values

We abide by the 7 cooperative principles:

1. **Voluntary and Open Membership**
We encourage organisations from the Commons and Social and Solidarity Economy to join as members to make use of the shared resource.
2. **Democratic Member Control**
Members have equal rights in the governance: one member equals one vote. Information about the project is shared transparently between members.
3. **Member Economic Participation**
Members are expected to contribute to the collective costs of running the project.
4. **Autonomy and Independence**
Initially the online meeting cooperative is an informal organisation formed by the member coops that have started the project. Built on mutual trust and intercooperation agreements, the project seeks to be a self-managed, autonomous organisation. Domains, assets and other shared resources will be transferred to a future independent legal entity, once the member organisations decide to create this.
5. **Education, Training, and Information**
A key objective of the project is to share knowledge and experience about running the shared services and facilitate its replication, though internal training, documentation, deployment scripts and a culture of sharing knowledge and software under free licenses with collaborative methodologies.
6. **Cooperation among Cooperatives**
The project was started by three cooperative, Collective.tools, femProcomuns and Webarchitects and other cooperative actors who seek to strengthen cooperation among themselves to enrich and strengthen the Social and Solidarity Economy.
7. **Concern for Community**
A driving force for this project is our care for privacy and Free Software, to allow people and organisations to have quality online meetings with ethical tools. The project has at its centre the community resource of online meeting services that are shared as a commons between its members. The generated software scripts and knowledge are shared as Free Software and under free licenses, following collaborative methodologies that facilitate replication and a decentralised architecture.