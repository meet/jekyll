---
title: Terms
---
---

## Billing

Members are required to make regular monthly contributions via [Open Collective](https://opencollective.com/meet-coop/). If you are unable to make your regular payment please [let us know](mailto:contact@meet.coop).

**When will I be billed?**

You will be charged when you first contribute, and then on the 1st of each month (for monthly contributions) or the 1st of the same month next year (for yearly contributions). Note: To prevent you from being charged twice in a short time frame, if you contribute after the 15th of a given month, the next charge will not be the 1st of next month but the month after.

**How can I change my payment method, payment amount or cancel my subscription?**

Please see the [payments section on Open Collective](https://docs.opencollective.com/help/financial-contributors/payments#payment-changes).

**Can I get a refund?**

Refunds on Open Collective are administered via our fiscal host (Platform 6). If you feel you deserve a refund please [email us](mailto:contact@meet.coop) explaining why and how much you would like refunded. We will not provide refunds for services which have been delivered in accordance with the Service Levels outlined on our website but will of course work with you to try and resolve any problems or discrepancies.

## Fair Use

**Short version:** Play nicely, respect other people, and we will all get along fine.

**Long version:** Meet.coop's fair use policy means that Members are expected to use Meet.coop's in line with their Membership levels and not abuse the services in any way. We run usage monitoring on our servers but do not actively police usage, nor do we want to. If we find that the severs are being overloaded we will investigate but we'd rather not have to because we'd rather be getting on with growing the co-op and improving our services. So please, play nicely and we'll all get along fine. If you do need more meeting participants than are allowed in your Membership Level, or use more resources than you feel you have paid for, please [make an additional donation via Open Collective](https://opencollective.com/meet-coop/donate).

## Privacy Policy

Please refer to the [privacy policy](https://wiki.meet.coop/wiki/Online_Meeting_Coop_Wiki:Privacy_policy), temporarily on our wiki.

## Disclaimer

Please refer to our [general disclaimer](https://wiki.meet.coop/wiki/Online_Meeting_Coop_Wiki:General_disclaimer), temporarily on our wiki.