---
title: Get involved with Meet.coop!
---

Meet.coop has two main categories of members. Anyone can apply to each membership type as an individual or as an organization. In the text below, “you” implies the individual or a member of the organization.

<h2>User Members</h2>

To join please select a service level from the table below.

- Once you have joined you may voluntarily participate in the Co-operative’s Circles.
- To maintain active membership, you must maintain good standing with membership fees and comply with the Meet.coop [terms of use](https://wiki.meet.coop/wiki/Online_Meeting_Coop_Wiki:General_disclaimer), which includes a Fair Use Agreement.
- User members can not resell, unless they are also an Operational Member.

| Level 1 | Level 2 | Level 3 | Multi-user Member |
| ------ | ------ | ------ | ------ |
| Maximum of 10 meeting participants | Maximum of 20 meeting participants | Maximum of 50 meeting participants | Unlimited user accounts, Max 100 meeting participants, Minimum of 3 months subscription |
| £9 / month | £18 / month | £40 / month | £90 / month |
| **[Join now](https://opencollective.com/meet-coop/contribute/member-level-1-19216/checkout)** | **[Join now](https://opencollective.com/meet-coop/contribute/member-level-2-19217/checkout)** | **[Join now](https://opencollective.com/meet-coop/contribute/member-level-3-19218/checkout)** | **[Join now](https://opencollective.com/meet-coop/contribute/mutli-user-member-19219/checkout)** |

<small>Prices are quoted in GBP, but will be charged in your local currency.</small>
<small>Membership is subject to a fair use and [privacy policy](https://wiki.meet.coop/wiki/Online_Meeting_Coop_Wiki:Privacy_policy) and the Meet.coop [terms of use](https://wiki.meet.coop/wiki/Online_Meeting_Coop_Wiki:General_disclaimer).</small>

<h2>Operational Members</h2>

- Operational Membership is open to Commons and Social and Solidarity Economy organisation who want to help develop and run Meet.coop who commit to contributing an annual membership in the form of time and/or money valued at €1,000, equivalent resources, or 40 hours of voluntary labour in work areas defined by the Co-operative. After start up, membership contributions for the second year are expected to be lower.
- In turn, you may participate in the Co-operative’s Circles and are eligible to apply for waged roles defined by the Co-operative. You may also resell our BigBlueButton server capacity via a set of Greenlight containers, under your administration, that correspond to the Service Levels defined by the Co-operative.
- To maintain active membership, you must be a contributor to at least one Circle and participate in at least 50% of our All Hands Meetings. If you are reselling Meet.coop services, you commit to pay a portion of your resale revenue, set by the Co-operative, to the Co-operative for covering its operation costs. You must also maintain a reasonable level of service to the User Members using Meet.coop services through your Greenlight containers, and provide best-effort oversight in compliance with the Co-operative’s Fair Use Agreement.
- All active Operational Members are entitled to a Greenlight account.

Operational Members will need to be approved by the existing members, please contact us via [contact@meet.coop](mailto:contact@meet.coop) to apply to become an Operational Member.

All memberships come with rights to participate in the Co-operative’s decision-making processes that correspond to the membership type and level of participation.

---

<h2>Find out more</h2>

**Wiki** - We are documenting the project at [wiki.meet.coop](https://wiki.meet.coop/).

**Forum** - We have an open discussion forum at [forum.meet.coop](https://forum.meet.coop/).

**Social Networks** - We encourage everyone to engage about The Online Meeting Cooperative in social networks. There’s no «official» account and we suggest to use the same hashtags to facilitate connections. **#meetcoop**

**Code** - We host all our code at the cooperative GitLab instance at [git.coop/meet](https://git.coop/meet).

**Contact** - If you want to contribute otherwise, please contact us at [contact@meet.coop](mailto:contact@meet.coop).
