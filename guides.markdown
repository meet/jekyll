---
title: How To Guides
---

Meet.coop is powered by [BigBlueButton](https://bigbluebutton.org/) - an open source web conferencing system.

## Testing BigBlueButton

You can test BigBlueButton on our demo server to see what it's like:

1. [Sign up for an account](https://demo.meet.coop/b/signup) on the demo server
2. Check your email for the "Welcome to BigBlueButton" email and click "Verify Account" - Check your spam folder if you don't see the email, it should arrive almost straight away
3. Sign in to your account using your email address and password
4. Once you're logged in you will see your Home Room (see image below)
5. Click "Start" to start your meeting
6. Invite other people to join you by sharing the "Invite participants" link 

![Greenlight](assets/img/greenlight.png)

### If you like the look of the demo please [sign up as a User Member](getinvolved.html)!

<hr>

## Using BigBlueButton

Watch the video below for a quick tour of the BigBlueButton tools.

<iframe width="650" height="365" src="https://www.youtube.com/embed/Hd4kGQ4db6Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<hr>

## Presenting on BigBlueButton

Watch the video below for a quick tour of the presentation controls on BigBlueButton.

<iframe width="650" height="365" src="https://www.youtube.com/embed/OL4ZuLabvYk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
